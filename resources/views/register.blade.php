<html>
	<head>
		<title>HTML Form</title>
	</head>
	<body>
		<form method="post" action="{{url('/welcome')}}">
			@csrf
			<h1>Buat Account Baru!</h1>
			<h2>Sign Up Form</h2>
			<div class="form-group">
				<label for="first">First name:</label>
				<input type="text" name="first">
			</div>
			<div class="form-group">
				<label for="last">Last name:</label>
				<input type="text" name="last">
			</div>
			<div class="form-group">
				<label>Gender:</label><br>
				<input type="radio" name="male"><label for="male">Male</label><br>
				<input type="radio" name="female"><label for="female">Female</label><br>
				<input type="radio" name="g_other"><label for="g_other">Other</label><br>
			</div>
			<div class="form-group">
				<label>Nationality:</label>
				<select>
					<option>Indonesian</option>
					<option>Singaporean</option>
					<option>Malaysian</option>
					<option>Australian</option>
				</select>
			</div>
			<div class="form-group">
				<label>Language Spoken</label>
					<input type="checkbox" name="indonesia"><label for="indonesia">Bahasa Indonesia</label><br>
					<input type="checkbox" name="english"><label for="english">English</label><br>
					<input type="checkbox" name="ls_other"><label for="ls_other">Other</label><br>
			</div>
			<div class="form-group">
				<label for="bio">Bio:</label>
				<textarea name="bio"></textarea>
			</div>
			<div class="form-group">
				<input type="submit" name="submit">
			</div>
		</form>
	</body>
</html>

